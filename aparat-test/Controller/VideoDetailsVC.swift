//
//  VideoDetailVC.swift
//  aparat-test
//
//  Created by Shayan Mehranpoor on 06/02/2018.
//  Copyright © 2018 Shayan Mehranpoor. All rights reserved.
//

import UIKit
import SDWebImage

class VideoDetailVC: UIViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        initVC()
    }
    
    func initVC() {
        self.title = "جزئیات ویدئو"
        customNavigationBar()
        
        let videoDetail = Bundle.main.loadNibNamed("VideoDetails", owner: self, options: nil)
        let videoDetailView = videoDetail?[0] as! VideoDetail
        videoDetailView.updateVideoList(video: detailVideo!)
        self.view = videoDetailView
    }
}

