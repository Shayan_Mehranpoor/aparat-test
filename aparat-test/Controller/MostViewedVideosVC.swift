//
//  ViewController.swift
//  aparat-test
//
//  Created by Shayan Mehranpoor on 06/02/2018.
//  Copyright © 2018 Shayan Mehranpoor. All rights reserved.
//

import UIKit

class MostViewedVideosVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var collectionView: UICollectionView!
    var mostViewdVideos = [MostViewedVideosDetails]()
    var isBigPoster = false
    var pagingForward = ""
    
    let pullToRefresh = PullToRefresh()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initVC()
        
        NotificationCenter.default.addObserver(self, selector: #selector(collectionViewReload), name:NSNotification.Name(rawValue: "reload"), object: nil)
        pullToRefresh.showPullToRefresh(collectionView: collectionView)
        
        squareLoading = initSquareLoading(view: view)
        getMostViewedVideos(url: "https://www.aparat.com//etc/api/mostviewedvideos") { (result) -> () in
            self.mostViewdVideos = result.mostViewedVideos!
            self.collectionView.reloadData()
            self.pagingForward = result.ui!.pagingForward!
            squareLoading.stop()
        }
    }
    
    func initVC() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 8, bottom: 75, right: 8)
        
        collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "MostViewdVideosLittleCell", bundle: nil), forCellWithReuseIdentifier: "LittleCell")
        collectionView.backgroundColor = UIColor.white
        self.view.addSubview(collectionView)
        
        self.title = "آپارات"
        self.customNavigationBar()
        addNavigationButtons()
    }
    
    func addNavigationButtons() {
        let leftItem = UIBarButtonItem(title: "نمایش", style: UIBarButtonItemStyle.plain, target: self, action: #selector(leftButtonTap))
        leftItem.isEnabled = true
        leftItem.setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: "IRANSans(FaNum)", size: 18.0)!, NSAttributedStringKey.foregroundColor: UIColor.white], for: .normal)
        self.navigationItem.setLeftBarButton(leftItem, animated: true)
    }
    
    @objc func leftButtonTap() {
        if isBigPoster {
            isBigPoster = false
            collectionView.register(UINib(nibName: "MostViewdVideosLittleCell", bundle: nil), forCellWithReuseIdentifier: "LittleCell")
            self.collectionView.reloadData()
        } else {
            isBigPoster = true
            collectionView.register(UINib(nibName: "MostViewdVideosBigCell", bundle: nil), forCellWithReuseIdentifier: "BigCell")
            self.collectionView.reloadData()
        }
    }
    
    @objc func collectionViewReload() {
        getMostViewedVideos(url: "https://www.aparat.com//etc/api/mostviewedvideos") { (result) -> () in
            self.mostViewdVideos.removeAll()
            self.mostViewdVideos = result.mostViewedVideos!
            self.collectionView.reloadData()
            self.pullToRefresh.refreshControl.endRefreshing()
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0, execute: {
                if (self.pullToRefresh.refreshControl.isRefreshing == true) {
                    self.pullToRefresh.refreshControl.endRefreshing()
                    print("end")
                }
            })
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mostViewdVideos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if !isBigPoster {
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LittleCell", for: indexPath) as? MostViewdVideosLittleCell {
                
                let video = mostViewdVideos[indexPath.row]
                cell.updateVideoList(video: video)
                
                return cell
                
            } else {
                return UICollectionViewCell()
            }
        } else {
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BigCell", for: indexPath) as? MostViewdVideosBigCell {
                
                let video = mostViewdVideos[indexPath.row]
                cell.updateVideoList(video: video)
                
                return cell
                
            } else {
                return UICollectionViewCell()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let videoDetailVC = VideoDetailVC() as UIViewController
        videoDetailVC.view.backgroundColor = UIColor.white
        detailVideo = mostViewdVideos[indexPath.row]
        self.navigationController?.pushViewController(videoDetailVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewSize = collectionView.frame.size.width

        if !isBigPoster {
            return CGSize(width: collectionViewSize, height: 115.0)
        } else {
            return CGSize(width: collectionViewSize, height: 300.0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.layer.transform = CATransform3DMakeScale(0.1, 0.1, 1)
        UIView.animate(withDuration: 0.3, animations: {
            cell.layer.transform = CATransform3DMakeScale(1, 1, 1)
        }, completion: { finished in
            UIView.animate(withDuration: 0.1, animations: {
                cell.layer.transform = CATransform3DMakeScale(1, 1, 1)
            })
        })
        
        if indexPath.row == (mostViewdVideos.count - 1) {
            squareLoading = initSquareLoading(view: view)
            getMostViewedVideos(url: self.pagingForward) { (result) -> () in
                for video in result.mostViewedVideos! {
                    self.mostViewdVideos.append(video)
                }
                self.collectionView.reloadData()
                self.pagingForward = result.ui!.pagingForward!
                squareLoading.stop()
            }
        }
    }
}

