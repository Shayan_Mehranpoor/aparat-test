//
//  Extensions.swift
//  aparat-test
//
//  Created by Shayan Mehranpoor on 06/02/2018.
//  Copyright © 2018 Shayan Mehranpoor. All rights reserved.
//

import UIKit
import AASquaresLoading

extension UIViewController {
    
    func initSquareLoading(view: UIView) -> AASquaresLoading {
        var loadingSquare = AASquaresLoading()
        if UIScreen.main.traitCollection.userInterfaceIdiom == .pad {
            loadingSquare = AASquaresLoading(target: view, size: 100.0)
        } else {
            loadingSquare = AASquaresLoading(target: view, size: 50.0)
        }
        loadingSquare.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        loadingSquare.color = UIColor.red
        
        loadingSquare.start()
        
        return loadingSquare
    }
    
    func customNavigationBar() {
        navigationController?.navigationBar.barTintColor = UIColor.gray
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "IRANSans(FaNum)", size: 20.0)!, NSAttributedStringKey.foregroundColor: UIColor.white]
    }
}
