//
//  VideoDetail.swift
//  aparat-test
//
//  Created by Shayan Mehranpoor on 06/02/2018.
//  Copyright © 2018 Shayan Mehranpoor. All rights reserved.
//

import UIKit
import SDWebImage

class VideoDetail: UIView {

    @IBOutlet weak var bigPoster: UIImageView!
    @IBOutlet weak var videoTitle: UILabel!
    @IBOutlet weak var videoVisit: UILabel!
    @IBOutlet weak var videoDuration: UILabel!
    
    func updateVideoList(video: MostViewedVideosDetails) {
        if video.bigPoster != nil {
            self.bigPoster.sd_setShowActivityIndicatorView(true)
            self.bigPoster.sd_setIndicatorStyle(.gray)
            self.bigPoster.sd_setImage(with: URL(string: video.bigPoster!))
        }
        self.videoTitle.text = "نام ویدئو: \(video.title!)"
        self.videoVisit.text = "تعداد بازدید: \(video.visitCount!) بار"
        self.videoDuration.text = "مدت زمان ویدئو: \(video.duration!) ثانیه"
    }
}
