//
//  MostViewdVideosBigCell.swift
//  aparat-test
//
//  Created by Shayan Mehranpoor on 06/02/2018.
//  Copyright © 2018 Shayan Mehranpoor. All rights reserved.
//

import UIKit
import SDWebImage

class MostViewdVideosBigCell: UICollectionViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var bigPoster: UIImageView!
    @IBOutlet weak var videoTitle: UILabel!
    @IBOutlet weak var videoCount: UILabel!
    @IBOutlet weak var videoDuration: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cellView.layer.cornerRadius = 4.0
        self.cellView.layer.borderWidth = 1.0
        self.cellView.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func updateVideoList(video: MostViewedVideosDetails) {
        if video.bigPoster != nil {
            self.bigPoster.sd_setShowActivityIndicatorView(true)
            self.bigPoster.sd_setIndicatorStyle(.gray)
            self.bigPoster.sd_setImage(with: URL(string: video.bigPoster!))
        }
        self.videoTitle.text = "نام ویدئو: \(video.title!)"
        self.videoCount.text = "تعداد بازدید: \(video.visitCount!) بار"
        self.videoDuration.text = "مدت زمان ویدئو: \(video.duration!) ثانیه"
    }
}
