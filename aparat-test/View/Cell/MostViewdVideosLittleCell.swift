//
//  MostViewdVideosCell.swift
//  aparat-test
//
//  Created by Shayan Mehranpoor on 06/02/2018.
//  Copyright © 2018 Shayan Mehranpoor. All rights reserved.
//

import UIKit
import SDWebImage

class MostViewdVideosLittleCell: UICollectionViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var smallPoster: UIImageView!
    @IBOutlet weak var videoTitle: UILabel!
    @IBOutlet weak var videoCount: UILabel!
    @IBOutlet weak var videoDuration: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cellView.layer.cornerRadius = 4.0
        self.cellView.layer.borderWidth = 1.0
        self.cellView.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func updateVideoList(video: MostViewedVideosDetails) {
        if video.smallPoster != nil {
            self.smallPoster.sd_setShowActivityIndicatorView(true)
            self.smallPoster.sd_setIndicatorStyle(.gray)
            self.smallPoster.sd_setImage(with: URL(string: video.smallPoster!))
        }
        self.videoTitle.text = "نام ویدئو: \(video.title!)"
        self.videoCount.text = "تعداد بازدید: \(video.visitCount!) بار"
        self.videoDuration.text = "مدت زمان ویدئو: \(video.duration!) ثانیه"
    }
}
