//
//  MostViewedVideos.swift
//  aparat-test
//
//  Created by Shayan Mehranpoor on 06/02/2018.
//  Copyright © 2018 Shayan Mehranpoor. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire
import AlamofireObjectMapper

class MostViewedVideos: Mappable {
    
    var mostViewedVideos: [MostViewedVideosDetails]?
    var ui: UI?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        mostViewedVideos <- map["mostviewedvideos"]
        ui <- map["ui"]
    }
}

class MostViewedVideosDetails: Mappable {
    
    var id: String?
    var title: String?
    var username: String?
    var userId: String?
    var visitCount: Int?
    var uid: String?
    var isHidden: Bool?
    var process: String?
    var bigPoster: String?
    var smallPoster: String?
    var profilePhoto: String?
    var duration: String?
    var sdate: String?
    var sdateTimeDiff: Int64?
    var frame: String?
    var official: String?
    var autoplay: Bool?
    var videoDateStatus: String?
    var degree: Bool?
    var deleteurl: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        username <- map["username"]
        userId <- map["userid"]
        visitCount <- map["visit_cnt"]
        uid <- map["uid"]
        isHidden <- map["isHidden"]
        process <- map["process"]
        bigPoster <- map["big_poster"]
        smallPoster <- map["small_poster"]
        profilePhoto <- map["profilePhoto"]
        duration <- map["duration"]
        sdate <- map["sdate"]
        sdateTimeDiff <- map["sdate_timediff"]
        frame <- map["frame"]
        official <- map["official"]
        autoplay <- map["autoplay"]
        videoDateStatus <- map["video_date_status"]
        degree <- map["360d"]
        deleteurl <- map["deleteurl"]
    }
}

class UI: Mappable {
    
    var pagingForward: String?
    var pagingBack: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        pagingForward <- map["pagingForward"]
        pagingBack <- map["pagingBack"]
    }
}

func getMostViewedVideos(url: String!, success: @escaping (_ result: MostViewedVideos) -> ()) {
    
    Alamofire.request(url, method: .get, encoding: JSONEncoding.default).responseObject { (response: DataResponse<MostViewedVideos>) in
        let getMostViewedVideos = response.result.value
        switch response.result {
        case .success:
            success(getMostViewedVideos!)
        case .failure(let error):
            print(error)
            squareLoading.stop()
        }
    }
}

